# Energy Management

## 1. Manage Energy not Time

### Question 1: What are the activities you do that make you relax - Calm quadrant?

- Music
- Video Games

#### Question 2: When do you find getting into the Stress quadrant?

- Procrastinate until the deadline is too close
- When I analyze I will not able to do a task, before even starting it

#### Question 3: How do you understand if you are in the Excitement quadrant?

- Finishing the given task before the deadline
- Achievements which lead to satisfaction

## 4. Sleep is your superpower

### Question 4: Paraphrase the Sleep is your Superpower video in detail

Lack of sleep has affect not only on mental health, but even physical health of a person. Sleep also works like a **save** button, so whatever we do throughout our day gets saved in to a temporary memory and sleep is needed to make that a permanent memory and it prepares the brain for learning new things the next day. There is a 40-percent deficit in the ability of the brain to make new memories without sleep.

We can't catch up on sleep, you can't accumulate a debt on sleep and then hope to pay it off at a later point in time. Under sleeping is catastrophic and our health deteriorates quickly, first, it's because human beings are the only species that deliberately deprive themselves of sleep for no apparent reason.

Sleep is the closest thing to elixir of immortality that we have.

### Question 5: What are some ideas that you can implement to sleep better?

- Sleep and wake up at the same time every day.
- Don't work in the room where you sleep.

## 5. Brain Changing Benefits of Exercise

### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points

- Exercise can help us focus better and build long term memory
- Exercise is the most transformative thing that can be done for brain.
  - Has immediate effects on the brain
  - Increase levels of Dopamine, and Serotonin
- Long-lasting effect can be achieved
  - Exercise creates new cells in Hippocampus
  - This improves long term memory
- Mood gets better
- Protective effects on the brain
  - Prefrontal cortex and bigger hippocampus
  - Reduce chances of Dementia and Alzheimer's disease
- Three to four sessions of exercise a week, at a minimum. For at least 30 minutes

### Question 7: What are some steps you can take to exercise more?

- Take stairs instead of lift
- Take walks daily
