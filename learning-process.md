# Learning Process

## Question 1

- What is the Feynman Technique?

### Answer 1

- Feynman Technique is a process to have a deep understanding of any topic. This technique states, if you want to understand something well, try to explain it in simple terms.</br>
By attempting to explain a concept in simple terms, we can quickly see where we have a good understanding of that concept. We’ll also be able to instantly pinpoint our problem areas, because they’ll be the areas where we either get stuck or we end up using complex language.</br>
This is a process of mainly 4 steps:

  1. **Study** - This step is pretty simple just choose a topic which we want to learn and start studyinga about it.

  2. **Teach** - Once we have completed the topic, now we have to teach it to someone else, or we can also teach to ourselve or some imaginary person by writing it down. The most important point here is to use simple language and not complex terms. We can imagine as we are teaching it to a kid. 

  3. **Fill in the gaps** -
  This step is to analyze where we get stuck while explaining. Analyze the gaps, weaknesses or mistakes in our explaining and then go back to the first step and study again and try to fill those gaps.

  4. **Simplify** - After we have corrected our mistakes or filled our gaps, it's time to simplify the language. This step is extremely effective at building our cohesive understanding of a subject. To be able to cut away clutter and explain something so clearly that even young kids with limited vocabulary can understand, is extremely difficult. Attempting to do this, forces us to not only deeply master the information/skill but to also grasp how all of the different elements join together.

## Question 2

- What are the different ways to implement this technique in your learning process?

### Answer 2

- I can improve my learning process by-
  
  - Studying the topic first, then try to explain it to myself.
  - Check if I am stuck somewhere, then go back to studying.
  - Try to simplify more.

## Question 3

- Paraphrase [the video](https://www.youtube.com/watch?v=O96fE1E-rf8) **in detail** in your own words.

### Answer 3

- **Learning how to learn** can be very effective when we try to learn something. Learning without knowing a proper way to learn can be time consuming and also not very effective. So, there are some key points to learning effectively. Our brain has mainly two modes focus mode and diffuse mode. Focus mode is turned on when we turn our attention to something but the diffuse mode is little different, it's a relax state. When we are learning our mind goes back and forth between these two modes. Suppose we have to learn some new concepts, our brain switches to focus mode, but there's a catch, if our brain is in focus mode, we can't think broadly as because focus mode is very tight, so to think broadly our brain switches to relax mode, that's where new ideas come and we can think broadly.

  **For example**- Thomas Edison also applied this technique and used it for learn better. He used to switch between focus and diffuse mode to understand, learn or solve any problem or topic. According to legend, he would sit in a chair with ball bearings in his hand, he would relax away, kind of thinking about the problem, loosely, that he was trying to solve related to his inventions, relaxing. Just as he'd fall asleep, the ball bearings would fall from his hands, and wake up, and he would take those ideas from the diffuse mode back into the focus mode. He'd use them to refine and finish his inventions.

  We can also use this technique in our daily life to improve our learning process and learn topics in a better way by switching between focus mode and diffuse mode effectively.

## Question 4

- What are some of the steps that you can take to improve your learning process?

### Answer 4

- I can improve my learning process by-

  - Using the technique of switching between focus mode and diffuse mode effectively as used by Sir, Thomas Edision.

  - Taking a small break every 25 minutes of work, so as to avoid procrastination.

## Question 5

- Your key takeaways from the [video](https://www.youtube.com/watch?v=5MgBikgcWnY)?

### Answer 5

- Key takeaways:

  - by following a specific method, anyone can learn a new skill in just 20 hours of dedicated practice.

  - importance of breaking a skill down into smaller and manageable chunks for effective practice.

  - importance of creating a practice plan and sticking to it, with a focus on deliberate practice.

  - importance of staying motivated during the learning process and using various techniques to stay engaged.

  - how to measure progress and adjust the practice plan as needed.

  - take a scientific and systematic approach to learning new skills, leading to faster and more efficient skill acquisition.

  - importance of understanding the core components of a skill before starting to practice.

## Question 6

- What are some of the steps that you can while approaching a new topic?

### Answer 6

- I can follow a specific pattern or a plan to learn a new skill.

- Being regular and consistent in learning a skill.

- Manage time accordingly. Make a plan on how much time I need to give for a skill to learn per day for a particular period of time and stick to the plan.

- First 20 hours is very important to learn a skill to a reasonable level.
