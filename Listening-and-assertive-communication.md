# Listening and Assertive Communication

## 1. Active Listening

### Question 1

- What are the steps/strategies to do Active Listening?

#### Answer:-

Active hearing is the act of fully hearing and understanding the meaning of what someone else is saying. Steps to do active listening :

- Not to get distracted by your own thoughts.
- Not to interrupt the other person while he is saying.
- Use door-opening phrases.
- Use proper body language to show you are actively listening.
- Take notes of important conversations.
- Paraphrase what others have said to make sure you are on the same page.

## 2. Reflective Listening

### Question 2

- According to Fisher's model, what are the key points of Reflective Listening?

#### Answer:-

Key points for reflective listening are :

- Seeking to get the idea that the speaker is trying to convey.
- Offering the idea back to the speaker, so to confirm that the idea is understood.
- It's also important to understand emotions too in what the other is saying.
- Respond with acceptance and empathy, not with indifference, cold objectivity, or fake concern.

### Question 3

- What are the obstacles in your listening process?

#### Answer:-

- Sometimes, I get distracted by my own thoughts and often miss what the other is saying.
- I don't use door openers often and don't.
- I often don't make eye contact while listening.

### Question 4

- What can you do to improve your listening?

#### Answer:-

- I should develop an interest in what the other is saying and try to connect with the person.
- I should do reflective listening to indulge more in what the other is saying.
- I can remind myself to look into the eye of the other person. 

### Question 5

- When do you switch to a Passive communication style in your day-to-day life?

#### Answer:-

- I often switch to passive communication when someone (mostly when elders) tries to force me to do something even if I don't want to do it.

### Question 6

- When do you switch to Aggressive communication styles in your day-to-day life?

#### Answer:-

I usually don't switch to aggressive communication. Sometimes, when someone talks to me rudely (someone younger than me)or tries to insult me then I generally switch to aggressive communication.

### Question 7

- When do you switch to Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?

#### Answer:-

I usually do it when someone does a silly mistake and not following the instructions.

### Question 8

- How can you make your communication assertive? 

#### Answer:-

- I should express my feelings better and try to recognize my needs.
- Understanding my limitations and trying to tell that to the other person.
- Use proper body language.