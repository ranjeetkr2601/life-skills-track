# Focus Management

## Question 1: What is Deep Work?

- Uninterrupted focus on work with **zero** distractions
- This helps to build skills and produce work of impeccable quality
- Strategies one can implement for deep work:-
  - Schedule breaks
  - Make deep work a habit or a sinusoidal function of time
  - Get adequete sleep

## Question 2: Paraphrase all the ideas in the above videos and this one in detail?

- Intense periods of focus produces myelin in relevant areas of brain, and it allows neurons to fire faster
- Ability to Deep Work is valuable and rare. Distractions happen every few minutes and break the flow of work which messes with our focus while working
- Distractions can be minimized by scheduling distractions
- Set boundaries
- Deep Work can be learnt. We can start it by scheduling it in phases and small durations around 60 to 90 minutes
- Early morning is best time to start deep working is in the morning when distractions are minimum
- Have shut down rituals. Create action plan for next day

## Question 3: How can you implement the principles in your day to day life ?

- Start with small duration of 60 minutes
- Keep all distraction at bay during those 60 minutes
- Do it everyday
- Gradually increase the duration of deep work sessions

## Question 4: Your key takeaways from the [video](https://www.youtube.com/watch?v=3E7hkPZ-HTk)

- Social media is highly dangerous weapon for killing and wasting time
- Distractions
- Unrealistic Expectations looking at others sharing life, truth or lie
- They are an entertainment product
- We are just products for ad agencies
- Designed to be addictive
- Fear of missing out.
