# Prevention of Sexual Harassment

## Question 1. What kinds of behaviour cause sexual harassment?

### Following behavior may cause sexual harassment

- Comments on the appearance of a person.
- Sexual jokes in person or group.
- Sending sexual messages, media, or emails to someone.
- Giving inappropriate gifts to coworkers or employees.
- Repeatedly asking for a date.
- Asking for nudes.
- Inappropriate touch.

## Question 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

In case I witness any incident once or repeatedly, I will first try to handle the situation at a personal level, I will give warning to the bully(s) to stop and behave appropriately, otherwise that person or group will have to face consequences.
Further, if the bully does not stop harassing me or someone at work, I will report the incident to my superiors or management team. In case for some reason they are unable to look into the matter, I will file an FIR against that person or group.
