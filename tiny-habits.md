# Tiny Habits

A funny clip to show the effect of tiny habits: [Jim's altoid prank on Dwight](https://www.youtube.com/watch?v=t4T4oGDW7bM).

## 1. Tiny Habits - BJ Fogg

### Question 1: Your takeaways from the video

- Design habits for the behaviour to achieve the outcome, not the outcome itself

- Have tiny habits which are designed to achieve an outcome. They take root in your system and compound the effect you put in to achieve the outcome
- To create a habit, one requires motivation, ability and a trigger

- Triggers can also have a cascading behaviour. Old behaviours can be used to trigger new behaviours

## 2. Tiny Habits by BJ Fogg - Core Message

### Question 2: Your takeaways from the video in as much detail as possible

- Behaviour = Motivation \* Ability \* Prompt

- Shrink the behaviour: Shrink habits to the tiniest possible version so that very little motivation is required to do it
  - Find a behaviour that is easy to do in 30 seconds or less
  - Find the tiniest habit with the biggest impact
  
- Identify a trigger

  - External Triggers
    - Too easy to ignore

  - Internal Triggers
    - Too easy to ignore

  - Action Triggers
    - Use the completion of one behaviour to trigger a new behaviour

- Grow your habit with some Shine
  - Start with something tiny and nurture it
  - The habit will naturally grow into a bigger habit
  - Shine: Authentic Pride, Celebrate when you achieve those tine habits every time you complete it

### Question 3: How can you use B = MAP to make making new habits easier?

- Start with a small habit which requires low motivation to complete, and we have the high ability to do it. Have behaviours which trigger those habits.

### Question 4: Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- In order to keep one motivated and pumped up, one should celebrate the successful completion of tasks. As this small celebration keeps the person motivated and encourages him to add up some complexity to the task.

## 3. 1% Better Every Day Video

### Question 5 Your takeaways from the video (Minimum 5 points)

- The idea of "minimum viable effort" which is the least amount of effort you need to put in to see progress.
- Give your goals a time and place in the world, and set a fixed time to execute said tasks.
- Make good habits easier to follow.
- Plan to optimize the start not the finish.
- Be consistent, and don't break the chain of small habits.

## 4. Book Summary of Atomic Habits

### Question 6: Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes

- The book suggested many ways of adapting any habit and making that habit your style of living, for doing this, the first step is to identify the habit and why you need that to adapt?, second step is to architect the path and generate a process to achieve your goal of adopting that habit, the third and last step is to reward your self with the outcomes.

### Question 7: Write about the book's perspective on how to make a good habit easier

The book suggests several strategies, such as:

- breaking down the habit into smaller
- manageable steps
- making the habit more

### Question 8: Write about the book's perspective on making a bad habit more difficult

To make a bad habit more difficult, the book suggests several strategies such as

- creating an obstacle to the habit
- designing the environment in a way that makes the habit less convenient
- replacing the bad habit with a new, more desirable habit.

## 5. Reflection

### Question 9: Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I will try to do atleast one coding problem each day to improve my logical thinking. I can do the question on leetcode.

### Question 10: Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- Sleeping late
- Try to avoid frequesnt distractions from devices such as smartphones.
