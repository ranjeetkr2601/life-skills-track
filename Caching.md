# **Caching**

## Abstract

A cache is a high-speed data storage layer that stores a subset of data, typically transient in nature, so that future requests for that data are served up faster than is possible by accessing the data’s primary storage location. Caching allows us to efficiently reuse previously retrieved or computed data. We need caching because fetching data from the main database can be slower which can affect the performance of our application. Caching is done to avoid redoing the same task again and again. Here we will talk about various caching techniques and how we can implement the best ones for our application.

## Discussion

Generally, cache memory has the **most recent** data accessed because there is a chance that data might be asked again but we also have to minimize the process of data retrieval from the database to improve performance. Therefore, the most recent data is not always the right choice. If we somehow know which data is to be accessed more frequently then our problem of caching will be solved but the most difficult task here is to predict that data which is to be stored in cache. Some terms related to cache-

+ When requested data is found in the cache, it's called **cache hit**. Successful cache results in a **high hit rate**.
+ When the requested data is not found in the cache it's called **cache miss**. It's a measure of poor caching technique.
+ Data becomes **stale** if the primary source of data gets updated and the cache doesn’t.

### Client-Side Caching

Client-level caching can be done so that the client does not need to request server-side. Similarly, the server may also use a cache. In that case, the server does not need to always hit the database for fetching data. We can have a cache between two components also.

### Handle Database pressure

Let’s imagine we are writing an article on a website. So, we have a browser that is the client of the system, and that particular website is the server. A user requests the server to write an article; then, it is stored in a database.

Now we may store the article in the server cache. So, we have two data sources for the same article. So, the question will be when to write to the database and when to write in the cache in case of an article edit. So, we need to know about caching invalidation techniques. Otherwise, we will get stale data for client requests.

## Types of Caching

1. **Write-through cache:** In this technique data is written both in the cache and database, but the data is updated in the cache first.</br>
    + Pros of this technique:
      + Cached data can be retrieved faster which will improve the performance.
      + As the data is written to both cache and database, we will always have a backup copy in case our system fails. Therefore, data will not be lost.

    + Cons of this technique:
      + We need to write data in two sources before the retrieval of data. Therefore, the write and update operation will have higher latency.

2. **Write-back cache:** In this technique data is written only to the cache. After data is written in the cache, a completion notification is sent to the client. The writing to the database is done after a time interval. This technique is useful when the application is write-heavy. And it provides low latency for such applications. You can reduce the frequency of database writes with this strategy.
    + Pros:
      + This technique is useful when the application is write-heavy. And it provides low latency for such applications. You can reduce the frequency of database writes with this strategy.
    + Cons:
      + As the cache is the only copy of the data and we have no backup of it in the database so, if the cache fails before updating the database, our data might get lost.

3. **Cache Aside:** In this technique, the cache works along with the database to reduce the hits on the database. When a request is received by the system, the system first looks for data in the cache. If data is found, then just return it. The database does not need to be involved. If the data is not found in the cache, then the data is retrieved from the database, the cache is updated with this data, and then is returned to the user.
    + Pros:
      + This approach works better for a read-heavy system and the data in the system is not frequently updated.

    + Cons:
      + The data present in the cache and the database could get inconsistent. To avoid this data on the cache has a TTL “Time to Live”. After the time interval, the data needs to be invalidated from the cache.

4. **Read Through Cache:** This is similar to the Cache Aside strategy. The difference is that the cache always stays consistent with the database. The cache library has to take the responsibility of maintaining consistency.

     + Pros:
       + This technique works better for a system with read-heavy tasks.

     + Cons:
       + The first time when information is requested by a user, it will be a cache miss. Then the system has to update the cache before returning the response. We can pre-load the cache with the information which has the chance to be requested most by the users.

## Conclusion

Caching is a key component to the performance of a system. A cache has limited storage and limited data from the main database. It is faster to retrieve data from a cache memory as compared to a database. Caches can be kept at all levels in the system, but we need to keep cache near the front end to quickly return the requested data. Caching is beneficial to performance, but it has some pitfalls. While designing a system, we need to be careful about the staleness of cache data.

## References

+ [Amazon AWS](https://aws.amazon.com/caching/)
+ [Youtube](https://www.youtube.com/watch?v=ccemOqDrc2I&t=316s)
+ [Developer.com](https://www.developer.com/design/performance-improvements-caching/)
+ [Medium- TowardsDataScience](https://towardsdatascience.com/system-design-basics-getting-started-with-caching-c2c3e934064a)
